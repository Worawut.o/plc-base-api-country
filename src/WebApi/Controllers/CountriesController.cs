﻿using Domain;
using Infra;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Controllers
{
    [Route("countries")]
    [ApiController]
    public class CountriesController : ControllerBase
    {
        public readonly DataContext _dataContext;
        public CountriesController(DataContext dataContext) {
            _dataContext = dataContext;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<IActionResult> CreateAsync([FromBody] CountryEntity country)
        {
            _dataContext.Countries.Add(country);
            await _dataContext.SaveChangesAsync();

            return CreatedAtRoute(null, null);
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<CountryEntity>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAsync()
        {
            var entities = await _dataContext.Countries.ToListAsync();
            return Ok(entities);
        }

        [HttpPut]
        [Route("{countryId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PutAsync(
            [FromRoute(Name = "countryId")] Guid countryId,
            [FromBody] CountryEntity country)
        {
            var entity = await _dataContext.Countries.FirstOrDefaultAsync(e => e.Id == countryId);
            if(entity == null) return NotFound();

            entity.Code2 = country.Code2;
            entity.Code3 = country.Code3;
            entity.Name = country.Name;
            entity.UpdateDate = country.UpdateDate;
            entity.CreateDate = country.CreateDate;
            
            await _dataContext.SaveChangesAsync();

            return NoContent();
        }

        [HttpPatch]
        [Route("{countryId}/name/{value}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PatchAsync(
            [FromRoute(Name = "countryId")] Guid countryId,
            [FromRoute(Name = "value")] string value)
        {
            var entity = await _dataContext.Countries.FirstOrDefaultAsync(e => e.Id == countryId);
            if (entity == null) return NotFound();

            entity.Name = value;
            entity.UpdateDate = DateTime.UtcNow;

            await _dataContext.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete]
        [Route("{countryId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteAsync([FromRoute(Name = "countryId")] Guid countryId)
        {
            var entity = await _dataContext.Countries.FirstOrDefaultAsync(e => e.Id == countryId);
            if (entity == null) return NotFound();

            _dataContext.Countries.Remove(entity);

            await _dataContext.SaveChangesAsync();

            return NoContent();

        }
    }
}

