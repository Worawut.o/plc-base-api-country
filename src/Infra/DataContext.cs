﻿using Domain;
using Microsoft.EntityFrameworkCore;

namespace Infra
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<CountryEntity> Countries { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CountryEntity>()
                .ToTable("Countries");

            modelBuilder.Entity<CountryEntity>()
            .HasKey(o => new { o.Id })
            .HasName("PkCountries");
        }
    }
}
